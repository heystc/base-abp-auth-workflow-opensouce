import request from '@/utils/request'

export function getAllStationCategorySelection() {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: `/api/services/app/stationCategory/GetAll`,
    method: `get`,
    params: { maxResultCount: 1000, skipCount: 0 }
  })
}

export function getStationPictures(data) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: `/api/services/app/StationPictures/GetPictures`,
    method: `get`,
    params: data
  })
}

export function deleteStationPicture(data) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: `/api/services/app/StationPictures/Delete`,
    method: `delete`,
    params: data
  })
}
