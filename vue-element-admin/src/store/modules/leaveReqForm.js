﻿import crud from './common/crudCodeBulid'

const leaveReqForm = {
  namespaced: true,
  state: {
    ...crud.state
  },
  mutations: {
    ...crud.mutations
  },
  actions: {
    ...crud.actions
  }
}

export default leaveReqForm
