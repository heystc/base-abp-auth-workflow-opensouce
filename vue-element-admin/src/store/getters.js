const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.profile.avatar,
  name: state => state.session.user.name,
  roles: state => state.session.user.name
}
export default getters
