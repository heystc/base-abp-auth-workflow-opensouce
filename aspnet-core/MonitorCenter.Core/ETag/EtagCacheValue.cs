﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MonitorCenter.ETag
{
    public class EtagCacheValue
    {
        public string ETag { get; set; }

        public string BodyText { get; set; }

    }
}
